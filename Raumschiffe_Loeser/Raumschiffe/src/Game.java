
public class Game {

    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta",1,1.0,1.0,1.0,1.0,2);
        Raumschiff romulaner = new Raumschiff("IRW Khazara",2,1.0,1.0,1.0,1.0,2);
        Raumschiff vulkanier = new Raumschiff("Ni'Var",0,0.8,0.8,0.5,1,5);

        klingonen.addLadung(new Ladung("Ferengi Schneckensaft",200));
        klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert",200));

        romulaner.addLadung(new Ladung("Borg-Schrott",5));
        romulaner.addLadung(new Ladung("Rote Materie", 2));
        romulaner.addLadung(new Ladung("Plasma-Waffe", 50));

        vulkanier.addLadung(new Ladung("Forschungssonde",35));
        vulkanier.addLadung(new Ladung("Photonentorpedo",3));

        klingonen.photonentorpedoSchiessen(romulaner);
        romulaner.phaserkanoneSchiessen(klingonen);
        vulkanier.nachrichtenAnAlle("Gewalt ist nicht logisch!");
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        klingonen.photonentorpedoSchiessen(romulaner);
        klingonen.photonentorpedoSchiessen(romulaner);
        
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        romulaner.zustandRaumschiff();
        romulaner.ladungsverzeichnisAusgeben();
        vulkanier.zustandRaumschiff();
        vulkanier.ladungsverzeichnisAusgeben();
        
    }
}
