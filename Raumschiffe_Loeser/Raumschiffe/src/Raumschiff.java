import java.util.ArrayList;

public class Raumschiff {
	// Attributes
	private String name;
	private int photontorpAmount;
	private double energy;
	private double shield;
	private double hull;
	private double lifeSupport;
	private int androidAmount;
	private static ArrayList<String> broadcastCommunicator;
	private ArrayList<Ladung> payloadList = new ArrayList<Ladung>();

	// Constructors

	public Raumschiff() {
	}

	public Raumschiff(String name, int photontorpAmount, double energy, double shield, double hull, double lifeSupport,
			int androidAmount) {
		this.name = name;
		this.photontorpAmount = photontorpAmount;
		this.energy = energy;
		this.shield = shield;
		this.hull = hull;
		this.lifeSupport = lifeSupport;
		this.androidAmount = androidAmount;

	}

	// Getter
	public String getName() {
		return this.name;
	}

	public int getPhotontorpAmount() {
		return this.photontorpAmount;
	}

	public double getShield() {
		return this.shield;
	}

	public double getEnergy() {
		return this.energy;
	}

	public double getHull() {
		return this.hull;
	}

	public double getLifeSupport() {
		return this.lifeSupport;
	}

	public int getAndroidAmount() {
		return this.androidAmount;
	}

	public static ArrayList<String> getBroadcastCommunicator() {
		return broadcastCommunicator;
	}

	// Setter

	public void setName(String name) {
		this.name = name;
	}

	public void setPhotontorpAmount(int photontorpAmount) {
		this.photontorpAmount = photontorpAmount;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
	}

	public void setShield(double shield) {
		this.shield = shield;
	}

	public void setHull(double hull) {
		this.hull = hull;
	}

	public void setLifeSupport(double lifeSupport) {
		this.lifeSupport = lifeSupport;
	}

	public void setAndroidAmount(int androidAmount) {
		this.androidAmount = androidAmount;
	}

	public static void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
		Raumschiff.broadcastCommunicator = broadcastCommunicator;
	}

	// Methods

	public void addLadung(Ladung newPayload) {
		payloadList.add(newPayload);

	}

	public void ladungsverzeichnisAusgeben() {
		for (Ladung payload : payloadList) {
			System.out.println(payload);
		}
	}

	public void zustandRaumschiff() {
		System.out.println("photonentorpedoAnzahl: " + this.photontorpAmount +
				"\nenergieversorgungInProzent: " + this.energy + 
				"\nschildeInProzent :" + this.shield +
				"\nhuelleInProzent: " + this.hull +
				"\nlebenesrhaltungssystemInProzent: "+ this.lifeSupport +
				"\nandroidenAnzahl: " + this.androidAmount +
				"\nschiffsName: " + this.name);
	}
	
	public void nachrichtenAnAlle(String nachricht) {
		System.out.println(nachricht);
	}

	public void photonentorpedoSchiessen(Raumschiff target) {
		if (this.photontorpAmount<=0) {
			nachrichtenAnAlle("-=*click+=-");
		} else {
			this.photontorpAmount--;
			nachrichtenAnAlle("Photonentorpedo abgeschossen!");
			treffer(target);
		}
	}
	private void treffer(Raumschiff target) {
		System.out.println(target.getName() + " wurde getroffen");
	}
	
	public void phaserkanoneSchiessen(Raumschiff target) {
		if(this.energy*100 > 50) {
			energy -= 0.5;
			nachrichtenAnAlle("Treffer mit Phaser");
			treffer(target);
		}else {
			nachrichtenAnAlle("-=*click+=-");
		}
	}
	
}
