import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;
import java.util.Collections;
import java.util.Scanner;

public class ArrayListAufgabe {

	public static void main(String[] args) {
		
		// In einer werden die Zufallszahlen hinzugef�gt
		// und in der andere werden die Indices an der 
		// die gesuchte Zahl sich befindet gespeichert.
		
		ArrayList<Integer> myList = new ArrayList<Integer>();
		ArrayList<Integer> myIndices = new ArrayList<Integer>();

		Scanner ms = new Scanner(System.in);

		// TODO: 2.: Deklaration von zwei Variablen f�r die beliebige Suchzahl und 
		// um die H�ufigkeit dieser Zahl in der Liste zu speichern.
		int zahl, zaehleZahl = 0;

		// TODO: 3. Erg�nzen Sie die Z�hlschleife. Diese soll die 20
		// zuf�llig erzeugten Zahlen in der Liste hinzuf�gen.
		
		for (int i = 0; i < 20; i++) {
			myList.add((int) (Math.random() * 8 + 1)); //Warum *8+1?
		}

		// TODO: 4. Geben Sie die Liste in der Konsole aus.
		for (int i = 0; i < myList.size(); i++) {
			System.out.printf("myList [%2d] : %3d\n", i, myList.get(i));
		}

		// TODO: 5. Eingabeaufforderung einer Zahl zwischen 1 und 9 und
		// Konsoleeingabe einlesen.

		do {
		System.out.println("Bitte geben Sie eine Zahl von 1 bis 9 ein: ");
		zahl = ms.nextInt();
		} while (zahl < 1 || zahl > 9);
		
		// TODO: 6. Ermittel Sie  wie oft sich die Zahl in der Liste befindet.
		// Verwenden Sie eine foreach Schleife.

		for (int i = 0 ; i < myList.size();i++) {
			if (myList.get(i) == zahl) {
				myIndices.add(i);
				zaehleZahl++;
			}
		}
		
		// TODO: 7. Geben Sie in der Konsole welche Suchzahl und wie oft sich diese Zahl 
		// in der Liste befindet aus.
		// Erg�nzen Sie die Ausgabe um die auszugebenden Variablen.
		
    	System.out.printf("\nDie Zahl %d kommt %d mal in der Liste vor.\n" , zahl , zaehleZahl);

		// TODO: 8. Suchen Sie die Indices, in der die eingegebende Zahl vorkommt und speichern
		// Sie diese in der Liste.
    	// Siehe TODO 6!

		// TODO: 9. Geben Sie die Gefundene Indices aus.

		System.out.printf("\nDie Zahl kommt an folgenden Indices in der Liste vor:\n");
		
		for (int i = 0 ; i < myList.size(); i++) {
			if (myList.get(i) == zahl) {
				System.out.println(i);
			}
		}
		
		System.out.println();
		
		// TODO: 10. L�schen Sie die gesuchte Zahl aus der Liste.
		
		for (int i = 0 ; i < myList.size(); i++) {
			if (myList.get(i) == zahl) {
				myList.remove(i);
				i--;
			}
		}
		
		// TODO: 11. Geben Sie die ver�nderte Liste aus.
		// Verwenden Sie eine Z�hlschleife.
		//System.out.println("\nListe nach L�schung von " + "________" + ": ");
		for (int i = 0; i < myList.size(); i++) {
			System.out.printf("myList [%2d] : %3d\n", i, myList.get(i));
		}
		// TODO: 12. F�gen Sie hinter jeder 5 eine 0 ein.

		for (int i = 0 ; i < myList.size(); i++) {
			if (myList.get(i) == 5) {
				myList.add(i+1,0);
			}
		}
		
		
		// TODO: 13. Geben Sie die ver�nderte Liste erneut aus.
		// Verwenden Sie eine Z�hlschleife. 
		System.out.println("\nListe nach Einfuegen von 0 hinter jeder 5");
		
		for (int i = 0; i < myList.size(); i++) {
			System.out.printf("myList [%2d] : %3d\n", i, myList.get(i));
		}
		
		ms.close();
	}

}
