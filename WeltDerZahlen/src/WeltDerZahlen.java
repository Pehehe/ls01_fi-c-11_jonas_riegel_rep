import java.util.Locale;

public class WeltDerZahlen {

	public static void main(String[] args) {
		/**
		  *   Aufgabe:  Recherechieren Sie im Internet !
		  * 
		  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
		  *
		  *   Vergessen Sie nicht den richtigen Datentyp !!
		  *
		  *
		  * @version 1.0 from 28.09.2021
		  * @author << Jonas Riegel >>
		  */
		    
		    /*  *********************************************************
		    
		         Zuerst werden die Variablen mit den Werten festgelegt!
		    
		    *********************************************************** */
		    // Im Internet gefunden ?
		    // Die Anzahl der Planeten in unserem Sonnesystem                    
		    int anzahlPlaneten = 8 ;
		    
		    // Anzahl der Sterne in unserer Milchstra�e
		    long anzahlSterne = 100000000000l;
		    
		    // Wie viele Einwohner hat Berlin?
		    int bewohnerBerlin = 3766082;
		    
		    // Wie alt bist du?  Wie viele Tage sind das?
		    
		    float alterTage = 365*30f;
		    
		    // Wie viel wiegt das schwerste Tier der Welt?
		    // Schreiben Sie das Gewicht in Kilogramm auf!
		    int gewichtKilogramm = 150000;
		    
		    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
		    int flaecheGroessteLand = 17098242;
		    
		    // Wie gro� ist das kleinste Land der Erde?
		    
		    float flaecheKleinsteLand = 0.44f;
		    
		    
		    
		    
		    /*  *********************************************************
		    
		         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
		    
		    *********************************************************** */
		    
		    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
		    
		    System.out.printf(Locale.GERMAN,"Anzahl der Sterne in der Milchstra�e: �ber %,d%n", anzahlSterne);
		    System.out.printf(Locale.GERMAN,"Anzahl der Bewohner in Berlin 2021:  %,d%n", bewohnerBerlin);
		    System.out.printf(Locale.GERMAN,"Mein Alter:  %.0f Jahre, das sind: %,.0f Tage%n", alterTage/365 , alterTage);
		    System.out.printf(Locale.GERMAN,"Das schwerste Tier der Welt wiegt: %,d Kg%n", gewichtKilogramm);
		    System.out.printf(Locale.GERMAN,"Das gr��te Land der Welt hat eine Gr��e von %,d km^2%n", gewichtKilogramm);
		    System.out.println("Das kleinste Land der Welt hat eine Gr��e von " + flaecheKleinsteLand + " km^2\n" );
		    
		    System.out.println(" *******  Ende des Programms  ******* ");

	}

}
