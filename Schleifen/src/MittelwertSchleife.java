import java.util.Scanner;

public class MittelwertSchleife {

	public static void main(String[] args) {
		//Deklaration von Variablen
		double rechnung = 0;
		int anzahl;
		
		//Eingabe
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert beliebig vieler Zahlen.");
		System.out.println("Bitte geben Sie an wie viele einzelne Zahlen Sie eingeben m�chten: ");
		anzahl = myScanner.nextInt();
		System.out.println("Bitte geben Sie die einzelnen Zahlen nacheinander ein und best�tigen jede mit Enter: ");
		
		for (int i = 1; i <= anzahl; i++) {
		System.out.println("Bitte geben Sie die " + i + ". Zahl ein: ");
		rechnung = rechnung + myScanner.nextDouble();
		}
		
		rechnung = rechnung / anzahl;
		System.out.print("Der Mittelwert betr�gt: " + rechnung);
		
		myScanner.close();
	}

}
