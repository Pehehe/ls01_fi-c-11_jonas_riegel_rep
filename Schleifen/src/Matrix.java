import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {

		int zeilen = 0;
		int spalten;
		int eingabe;
		
		//Eingabe
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm erstellt eine 10x10 Matrix mit den Zahlen von 1 bis 100, wobei Zahlen, die folgendenden Regeln entsprechen, durch ein \"*\" ersetzt sind: ");
		System.out.println("1) Die vom Benutzer eingegebene Zahl enthalten ");
		System.out.println("2) Durch die vom Benutzer eingegebene Zahl restlos teilbar sind ");
		System.out.println("3) Deren Quersumme der vom Benutzer eingegebenen Zahl entsprechen ");
		System.out.println(" ");
		System.out.println("Bitte geben Sie eine ganze Zahl von 2 bis 9 ein: ");
		eingabe = myScanner.nextInt();

		while (zeilen < 10) {
			spalten = 0;
			do {
				if (spalten == eingabe) {
					System.out.print(" * ");
					spalten = spalten + 1;
				}
				else if (zeilen == eingabe) {
					System.out.print(" * ");
					spalten++;
				}
				else if ((zeilen + spalten) == eingabe) {
					System.out.print(" * ");
					spalten = spalten + 1;
					}
				else if((zeilen * 10 + spalten)!=0 && (zeilen * 10 + spalten)%eingabe == 0){
					System.out.print(" * ");
					spalten = spalten + 1;
					}
				else {
				System.out.print(zeilen);
				System.out.print(spalten);
				System.out.print(" ");
				spalten = spalten + 1;
				}
			}while (spalten < 10);
			
			System.out.println();
			zeilen++;
		};

		myScanner.close();

	}

}
