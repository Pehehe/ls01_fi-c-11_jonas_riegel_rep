import java.util.Scanner;

public class Treppe {

	public static void main(String[] args) {

		int hoehe = 0;
		int sBreite = 0;		
		
		//Eingabe
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm erstellt grafisch eine Treppe nach Benutzervorgabe f�r H�he und Breite.");
		System.out.println("Bitte geben Sie die gew�nschte H�he in Zeilen der Treppe an: ");
		hoehe = myScanner.nextInt();
		System.out.println("Bitte geben Sie die gew�nschte Stufenbreite der Treppe an: ");
		sBreite = myScanner.nextInt();
		
		//Ausgabe
		for (int i = 1; i <= hoehe; i++) {
			System.out.println();
			
			for (int j = (hoehe * sBreite) - (i * sBreite); j > 0; j--) {
				System.out.print(" ");				
			}
			for (int k = (i * sBreite); k > 0; k--) {
				System.out.print("*");	
			}
			
		}
		myScanner.close();
	}
		
}
