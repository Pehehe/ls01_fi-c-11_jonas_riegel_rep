import java.util.Locale;

//Der Import ist laut Aufgabenstellung eigentlich nicht zul�ssig, aber notwendig, damit "Locale.US" der printf funktion ausgef�hrt wird.
//"Locale.US" wird ben�tigt, damit auf jedem System die Ausgabe der rechten Spalte der Tabelle mit einem "." erfolgt und nicht wie bei einem bspw. deutschem System mit einem ","
//Alternativ besteht die M�glichkeit in Eclipse selbst die Location und Sprache auf US/Englisch zu stellen, dann erfolgt die Ausgabe aber nicht auf jedem System gleich.

public class A1_6_ab3 {

	public static void main(String[] args) {
		
		int z1l = -20; 
		int z2l = -10;
		int z3l = 0;
		int z4l = 20;
		int z5l = 30;
		
		double z1r = -28.8889;
		double z2r = -23.3333;
		double z3r = -17.7778;
		double z4r = -6.6667;
		double z5r = -1.1111;
		

		System.out.printf("%-12s|%10s%n", "Fahrenheit" , "Celsius");
		System.out.println("-----------------------");
		System.out.printf(Locale.US,"%+-12d|%10.2f%n", z1l , z1r );
		System.out.printf(Locale.US,"%+-12d|%10.2f%n", z2l , z2r );
		System.out.printf(Locale.US,"%+-12d|%10.2f%n", z3l , z3r );
		System.out.printf(Locale.US,"%+-12d|%10.2f%n", z4l , z4r );
		System.out.printf(Locale.US,"%+-12d|%10.2f%n", z5l , z5r );
		

	}

}