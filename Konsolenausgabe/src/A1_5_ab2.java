
public class A1_5_ab2 {

	public static void main(String[] args) {
		
		//Version 1: übersichtlicher Code
		
		String s = "*************";
		
		System.out.printf( "%7.1s%n", s );
		System.out.printf( "%8.3s%n", s );
		System.out.printf( "%9.5s%n", s );
		System.out.printf( "%10.7s%n", s );
		System.out.printf( "%11.9s%n", s );
		System.out.printf( "%12.11s%n", s );
		System.out.printf( "%13s%n", s );
		System.out.printf( "%8.3s%n", s );
		System.out.printf( "%8.3s%n%n%n", s );
		
		//Version 2: weniger übersichtlich im Code, aber gleiches Ergebnis
		
		//System.out.printf( "%7.1s%n%8.3s%n%9.5s%n%10.7s%n%11.9s%n%12.11s%n%13s%n%8.3s%n%8.3s%n", s , s , s , s , s , s , s , s , s);
		
	}

}
