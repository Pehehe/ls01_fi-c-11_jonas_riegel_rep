﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)    {
    	Scanner tastatur = new Scanner(System.in);
    	char r = 'n';
    	
    	do {
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
    	
    	double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
    	
    	int zaehler = 8;
    	
    	fahrkartenAusgeben(zaehler);
    	
    	rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    	
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n\n");

       System.out.print("Bitte eine beliebige Eingabe tätigen um das Programm neu zu starten (Eingabe \"a\" für Abbruch):");
       r = tastatur.next().charAt(0);
       
       System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
       
    	}while(r != 'a');
       tastatur.close();
    }
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur) {
    	
    	System.out.print("Fahrkartenbestellvorgang: \n=========================\n\n");
    	
    	int auswahl;
    	int anzahlTickets;
    	double zwischensumme = 0;
    	double zuZahlenderBetrag = 0;

    	while (true) {
        	System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus: ");
        	System.out.println("  Einzelfahrschein			Regeltarif AB [ 2,90 €]	(1)");
        	System.out.println("  Tageskarte				Regeltarif AB [ 8,60 €]	(2)");
        	System.out.println("  Kleingruppen-Tageskarte		Regeltarif AB [23,50 €] (3)");
        	System.out.println("  Bezahlen							(9)\n\n");
    		System.out.print("Ihre Wahl: ");
    		auswahl = tastatur.nextInt();
    		
	    	switch(auswahl) {
	    	case 1:
	    		zwischensumme = 2.90;
	    		break;
	    	case 2:
	    		zwischensumme = 8.60;
	    		break;
	    	case 3:
	    		zwischensumme = 23.50;
	    		break;
	    	case 9:
	    		if(zwischensumme != 0) {
		    		return zuZahlenderBetrag;
	    		}
	    		System.out.println("\nBitte wählen Sie erst eine Karte aus bevor Sie zum Bezahlen gehen.\n\n");
	    		break;
	    	default:
	    		System.out.println(" >>falsche Eingabe<<");
	    	}
	    	
	    	if(zwischensumme != 0) {
	    	System.out.print("Anzahl der Tickets: ");
	    	anzahlTickets = tastatur.nextInt();
	    		
		    if (anzahlTickets < 1 || anzahlTickets > 10) {
		       	System.out.println("Ungültige Auswahl an Tickets. Es wird der Standardwert 1 gewählt.");
		       	anzahlTickets = 1;
		    }
		    zuZahlenderBetrag = zuZahlenderBetrag + (zwischensumme * anzahlTickets);
		    System.out.println("\nDie Zwischensumme beträgt: " + zuZahlenderBetrag + "\n\n");
	    	}
    	}
    }
    
    public static double fahrkartenBezahlen(double zuZahlen, Scanner tastatur) {
    	
        double eingezahlterGesamtbetrag = 0;
        double eingeworfeneMünze;
        
        while(zuZahlen > eingezahlterGesamtbetrag)
        {
     	   System.out.printf("\nNoch zu zahlen: %.2f €%n" , (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben(int zaehler) {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < zaehler; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
        System.out.println("\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	double rückgabebetrag = (eingezahlterGesamtbetrag - zuZahlenderBetrag);
        
    	if(rückgabebetrag > 0.00)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO wird in folgenden Münzen gezahlt: \n", rückgabebetrag);

            while(rückgabebetrag > 1.99) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.00;
            }
            while(rückgabebetrag > 0.99) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.00;
            }
            while(rückgabebetrag > 0.49) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	  rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag > 0.19) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag > 0.09) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag > 0.04)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }
}