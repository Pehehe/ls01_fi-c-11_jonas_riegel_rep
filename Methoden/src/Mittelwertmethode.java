import java.util.Scanner;

public class Mittelwertmethode {

		public static void main(String[] args) {
			double zahl1;
			double zahl2;
			double mittelwert;
			
			Scanner myScanner = new Scanner(System.in);
			zahl1 = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ");
			zahl2 = eingabe(myScanner, "Bitte geben Sie die zweite Zahl ein: ");
			mittelwert = verarbeitung(zahl1, zahl2);
			ausgabe(mittelwert);
			myScanner.close();
	}

	
		public static double eingabe (Scanner myScanner, String text) {
		
			System.out.println(text);
			double eingabe = myScanner.nextDouble();
			return eingabe;
		}
		
		public static double verarbeitung (double zahl1, double zahl2) {
			double m = (zahl1 + zahl2)/ 2.0;
			return m;
		}
		
		public static void ausgabe (double mittelwert) { //mittelwert muss nicht diesen Namen haben, m g�nge auch, da �bergebene mittelwert nicht die Variable ist
			System.out.println("Mittelwert: " + mittelwert);
		}

	}