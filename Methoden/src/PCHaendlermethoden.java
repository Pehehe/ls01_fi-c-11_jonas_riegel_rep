import java.util.Scanner;

public class PCHaendlermethoden {

	public static void main(String[] args) {
		
		String artikel;
		int anzahl;
		double nettopreis;
		double gnettopreis;
		double mwst;
		double gbruttopreis;
		Scanner myScanner = new Scanner(System.in);
		
		artikel = liesString (myScanner, "Was möchten Sie bestellen? "); // Reihenfolge hier muss mit der der Methode übereinstimmen, also ich übergebe myScanner und text, also muss bei methode Scanner und String stehen
		anzahl = liesint (myScanner, "Geben Sie die Anzahl ein: ");
		nettopreis = liesdouble (myScanner, "Geben Sie den Nettopreis ein: ");
		mwst = liesdouble (myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein: ");
		gnettopreis = berechneGesamtnettopreis (anzahl, nettopreis);
		gbruttopreis = berechneGesamtbruttopreis (gnettopreis, mwst);
		rechnungsausgabe (artikel, anzahl, gnettopreis, gbruttopreis, mwst);
		myScanner.close();

	}
	
	public static String liesString (Scanner myScanner, String text) {
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesint (Scanner myScanner, String text) {
		System.out.println(text);
		int liesint = myScanner.nextInt();
		return liesint;
	}
	
	public static double liesdouble (Scanner myScanner, String text) {
		System.out.println(text);
		double liesdouble = myScanner.nextDouble();
		return liesdouble;
	}
	
	public static double berechneGesamtnettopreis (int anzahl, double nettopreis) {
		double gesamtnettopreis = anzahl * nettopreis;
		return gesamtnettopreis;
	}
	
	public static double berechneGesamtbruttopreis (double nettogesamtpreis, double mwst) {
		double gesamtbruttopreis = nettogesamtpreis * (1 + mwst / 100);
		return gesamtbruttopreis;		
	}
	
	public static void rechnungsausgabe (String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
	}
}
