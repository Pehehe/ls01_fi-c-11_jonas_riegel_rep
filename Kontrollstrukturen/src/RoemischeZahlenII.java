import java.util.Locale;
import java.util.Scanner;

public class RoemischeZahlenII {

	public static void main(String[] args) {
		
		String roemisch;			//String der R�mischen Zahl
		int zaehler = 0;			//Z�hler f�r Stelle des Strings
		double ausgabe = 0;			//double um Quotient f�r Bedingung bilden zu k�nnen
		boolean abbruch = false;
		Scanner myScanner = new Scanner(System.in);
		
		roemisch = eingabe(myScanner);
		myScanner.close();
		
		if (control(roemisch) == true) {
			abbruch = true;
		}
		
		while (abbruch == false && zaehler < roemisch.length()) {
			if (zaehler == roemisch.length() - 1) {
				ausgabe = ausgabe + umwandlung(roemisch, zaehler);
				break;
			}
			switch(roemisch.charAt(zaehler)){
				case 'I': 
				case 'X':
				case 'C': 
				case 'M':
					//Switch Nachfolger betrachten
					switch (roemisch.charAt(zaehler + 1)) {
					case 'V':
					case 'L':
					case 'D':
						//Subtraktion einleiten
						if ((umwandlung(roemisch, zaehler) / umwandlung(roemisch, zaehler+1) == 0.2) || (umwandlung(roemisch, zaehler) / umwandlung(roemisch, zaehler+1) == 0.1)){
							ausgabe = ausgabe + umwandlung(roemisch, zaehler + 1) - umwandlung(roemisch, zaehler);
							zaehler += 2;				
							break;
						}
					case 'I':
					case 'X':
					case 'C':
					case 'M':
						//Mehrfache Addition einleiten
						if (umwandlung(roemisch, zaehler) == umwandlung(roemisch, zaehler + 1)) {
							ausgabe = ausgabe + umwandlung(roemisch, zaehler);
							zaehler += 1;
							break;
						}
						//Subtraktion einleiten
						else if((umwandlung(roemisch, zaehler) / umwandlung(roemisch, zaehler+1) == 0.2) || (umwandlung(roemisch, zaehler) / umwandlung(roemisch, zaehler+1) == 0.1)){
							ausgabe = ausgabe + umwandlung(roemisch, zaehler + 1) - umwandlung(roemisch, zaehler);
							zaehler += 2;
							break;
							}
		
						//Einfache Addition einleiten
						else if(umwandlung(roemisch, zaehler) > umwandlung(roemisch, zaehler+1)) {
							ausgabe = ausgabe + umwandlung(roemisch, zaehler);
							zaehler += 1;
							break;
							}
					}
					break;
				case 'V':
				case 'L':
				case 'D':
					ausgabe = ausgabe + umwandlung(roemisch, zaehler);
					zaehler += 1;
					break;
			}
		}
		ausgabe(ausgabe);
	}
	
	public static String eingabe(Scanner myScanner) {
		System.out.println("Bitte geben Sie eine R�mische Zahl zwischen 1 und 3999 ein");
		String s = myScanner.next();		
		return s;
	}
	
	public static boolean control(String roemisch) {
		boolean r = false;
		int i = 0;
		
		while (i < roemisch.length() && r == false) {
			
			if( i > 2 && umwandlung(roemisch, i-1) > umwandlung(roemisch, i-2) && umwandlung(roemisch, i) >= (umwandlung(roemisch, i-2))) {
				System.out.println("Falsche Eingabe: Ein Zeichen darf nach einer Subtraktion nicht wieder addiert werden.");
				r = true;
				break;
			};
			
			switch (roemisch.charAt(i)) {
			case 'I':
			case 'X':
			case 'C':
			case 'M':
				//3x gleiche Check
				if (i < roemisch.length() - 3 && roemisch.charAt(i) == roemisch.charAt(i + 1) && roemisch.charAt(i) == roemisch.charAt(i + 2) && roemisch.charAt(i) == roemisch.charAt(i + 3)) {
					System.out.println("Falsche Eingabe: die Zeichen I, X, C und M d�rfen nur maximal 3x aufeinander folgen.");
					r = true;
				}
				//Check ob Vorg�nger kleiner oder gleich dem Nachfolger ist - Nur bei I, X, C und M
				else if(i < roemisch.length() - 1 && i > 0 && umwandlung(roemisch, i - 1) <= umwandlung(roemisch, i + 1)) {
					//Check ob Nachfolger V, L, D ist
					if (roemisch.charAt(i + 1) == 'V' || roemisch.charAt(i + 1) == 'L' || roemisch.charAt(i + 1) == 'D') {
						System.out.println("Falsche Eingabe: Eine Subtraktion von V, L oder D darf nicht auf V, L oder D folgen.");
						r = true;
					}
				}
				break;
			case 'V':
			case 'L':
			case 'D':
				//Mehrfache V,L,D Check
				if (i < roemisch.length() - 1 && roemisch.charAt(i) == roemisch.charAt(i + 1)) {
					System.out.println("Falsche Eingabe: die Zeichen V, L und D k�nnen nicht aufeinander folgen.");
					r = true;
				}
				break;
			default:
				//Kein richtiges Zeichen an einer Stelle
				System.out.println("Bitte nur Buchstaben eingeben, die f�r R�mische Zahlen verwendet werden!");
				r = true;
				break;				
			}
			//Unzul�ssiger Subtrahent
			if (i < roemisch.length() - 1 && umwandlung(roemisch, i) < umwandlung(roemisch, i + 1) && umwandlung(roemisch, i) / umwandlung(roemisch, i + 1) != 0.1 && (umwandlung(roemisch, i) / umwandlung(roemisch, i + 1) != 0.2)) {
				System.out.println("Falsche Subtraktionssyntax: Die Differenz der Subtrahenten darf nur 0.1 oder 0.2 sein.");
				r = true;
				break;
			}
			if (i < roemisch.length() - 1 && umwandlung(roemisch, i) < umwandlung(roemisch, i + 1) && (roemisch.charAt(i) == 'V' || roemisch.charAt(i) == 'L' || roemisch.charAt(i) == 'D')) {
				System.out.println("Falsche Subtraktionssyntax: der Subtrahend darf nur I, X oder C sein.");
				r = true;
				break;
			}
			//Weitere: Subtraktion pr�fen
			if (roemisch.length() - 1 > i && i > 0 && umwandlung(roemisch, i) < umwandlung(roemisch, i + 1)) {
				//Mehrfacher Abzug eines kleineren von einem gr��eren Zeichen 
				if (umwandlung(roemisch, i) >= umwandlung(roemisch, i - 1)) {
					System.out.println("Falsche Eingabe: Es darf nur 1x eine kleinere Zahl vor einer gr��eren stehen.");
					r = true;
					break;
				}
				//Check nach Subtraktion kein gr��eres Zeichen als vor der Subtraktion
				else if (umwandlung(roemisch, i - 1) < umwandlung(roemisch, i + 1)) {
					System.out.println("Falsche Eingabe: Das Zeichen vor der Differenz muss gr��er als der Minuend sein.");
					r = true;
					break;
				}
			}
			//Weitere Zahlensysntax pr�fen
			if (roemisch.length() - 2 > i && i > 0 && umwandlung(roemisch, i) < umwandlung(roemisch, i +1) && umwandlung(roemisch, i + 1) == umwandlung(roemisch, i + 2)) {
				System.out.println("Falsche Eingabe: Nach dem Minuend darf nicht noch einmal das gleiche Zeichen folgen.");
				r = true;
				break;
			}
			i = i+1;
		}
		return r;
	}
	
	public static double umwandlung(String eingabe, int zaehler) {
		double r = 0;
		switch(eingabe.charAt(zaehler)){
			case 'I':
				r = 1;
				break;
			case 'V':
				r = 5;
				break;
			case 'X':
				r = 10;
				break;
			case 'L':
				r = 50;
				break;
			case 'C':
				r = 100;
				break;
			case 'D':
				r = 500;
				break;
			case 'M':
				r = 1000;
				break;		
			default:
				r = 0;
		}		
		return r;	
	}

	public static void ausgabe(Double ausgabe) {
		System.out.printf(Locale.US,"Die umgerechnete Dezimalzahl betr�gt: %.0f%n", ausgabe );
	}
}